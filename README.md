PROAUTO
=======

Aplicativo para envio de comandos PROAUTO

Detalhes de versões:

1.0 - Envio de comandos por mensagem;
1.1 - Envio de mensagem sem registro na caixa de mensagens;
2.0 - Novo visual;
2.0.5 - Armazenamento do número do chip nas preferências;
