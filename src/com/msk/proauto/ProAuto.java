package com.msk.proauto;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class ProAuto extends Activity implements OnClickListener {

	private TextView liga, desliga;
	private EditText numero, senha;
	private CheckBox corte, sirene, escuta, aviso;
	private String nrCHIP, senhaPROAUTO, comando, comando0, comando1, comando2,
			numeroCHIP;
	private int alternador;

	SharedPreferences buscaPreferencias = null;
	Boolean UsaNumeroCHIP = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.inicio);

		liga = (TextView) findViewById(R.id.tvLIGA);
		desliga = (TextView) findViewById(R.id.tvDESLIGA);

		numero = (EditText) findViewById(R.id.etChip);
		senha = (EditText) findViewById(R.id.etSenha);

		buscaPreferencias = PreferenceManager.getDefaultSharedPreferences(this);

		UsaNumeroCHIP = buscaPreferencias.getBoolean("numero", false);
		numeroCHIP = buscaPreferencias.getString("chip", "");

		if (UsaNumeroCHIP == true) {
			numero.setText(numeroCHIP);
		}

		nrCHIP = senhaPROAUTO = comando = comando0 = comando1 = comando2 = "";

		corte = (CheckBox) findViewById(R.id.cbCorte);
		sirene = (CheckBox) findViewById(R.id.cbSirene);
		escuta = (CheckBox) findViewById(R.id.cbEscuta);
		aviso = (CheckBox) findViewById(R.id.cbAviso);

		liga.setOnClickListener(this);
		desliga.setOnClickListener(this);

	}

	@Override
	public void onClick(View botao) {

		nrCHIP = numero.getText().toString();
		senhaPROAUTO = senha.getText().toString();

		switch (botao.getId()) {

		case R.id.tvLIGA:

			alternador = 1;

			if (corte.isChecked())
				comando = "B";

			if (sirene.isChecked())
				comando = "C";

			if (sirene.isChecked() && corte.isChecked()) {
				comando0 = "D";
				comando = "";
			}

			if (escuta.isChecked())
				comando1 = "L";

			if (aviso.isChecked())
				comando2 = "T1";

			break;
		case R.id.tvDESLIGA:

			alternador = 0;

			if (corte.isChecked())
				comando = "A";

			if (sirene.isChecked())
				comando = "A";

			if (sirene.isChecked() && corte.isChecked()) {
				comando = "";
				comando0 = "A";
			}
			if (escuta.isChecked())
				comando1 = "K";

			if (aviso.isChecked())
				comando2 = "T0";

			break;

		}

		if (numero.getText().equals("")) {

			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.aviso_sem_numero),
					Toast.LENGTH_SHORT).show();

		} else if (senha.getText().equals("")) {

			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.aviso_sem_senha),
					Toast.LENGTH_SHORT).show();

		} else if (comando.equals("") && comando0.equals("")
				&& comando1.equals("") && comando2.equals("")) {

			Toast.makeText(getApplicationContext(),
					getResources().getString(R.string.aviso_sem_comando),
					Toast.LENGTH_SHORT).show();

		} else {

			EnviaComandoSelecionado();
			NotificaUsuario(alternador);
		}

		corte.setChecked(false);
		sirene.setChecked(false);
		escuta.setChecked(false);
		aviso.setChecked(false);

		comando = "";
		comando0 = "";
		comando1 = "";
		comando2 = "";

		numero.setText("");
		senha.setText("");
	}

	private void EnviaComandoSelecionado() {
		// DEFINE O COMANDO QUE SERA ENVIADO
		if (!comando.equals("")) {

			enviaSMS(nrCHIP, "VIAS" + senhaPROAUTO + comando);
		}

		if (!comando0.equals("")) {

			enviaSMS(nrCHIP, "VIAS" + senhaPROAUTO + comando0);
		}

		if (!comando1.equals("")) {

			enviaSMS(nrCHIP, "VIAS" + senhaPROAUTO + comando1);
		}

		if (!comando2.equals("")) {

			enviaSMS(nrCHIP, "VIAS" + senhaPROAUTO + comando2);
		}

	}

	private void NotificaUsuario(int opcao) {
		// DEFINE NOTIFICACAO PARA USUARIO

		if (opcao == 1) {

			if (corte.isChecked() && sirene.isChecked()) {

				Toast.makeText(
						getApplicationContext(),
						getResources().getString(
								R.string.aviso_liga_corte_sirene),
						Toast.LENGTH_SHORT).show();

			} else if (corte.isChecked()) {

				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.aviso_liga_corte),
						Toast.LENGTH_SHORT).show();

			} else if (sirene.isChecked()) {

				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.aviso_liga_sirene),
						Toast.LENGTH_SHORT).show();

			}

			if (aviso.isChecked()) {

				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.aviso_liga_aviso),
						Toast.LENGTH_SHORT).show();

			}

			if (escuta.isChecked()) {

				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.aviso_liga_escuta),
						Toast.LENGTH_SHORT).show();

			}

		} else {

			if (corte.isChecked() && sirene.isChecked()) {

				Toast.makeText(
						getApplicationContext(),
						getResources().getString(
								R.string.aviso_desliga_corte_sirene),
						Toast.LENGTH_SHORT).show();

			} else if (corte.isChecked()) {

				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.aviso_desliga_corte),
						Toast.LENGTH_SHORT).show();

			} else if (sirene.isChecked()) {

				Toast.makeText(
						getApplicationContext(),
						getResources().getString(R.string.aviso_desliga_sirene),
						Toast.LENGTH_SHORT).show();

			}

			if (aviso.isChecked()) {

				Toast.makeText(getApplicationContext(),
						getResources().getString(R.string.aviso_desliga_aviso),
						Toast.LENGTH_SHORT).show();

			}

			if (escuta.isChecked()) {

				Toast.makeText(
						getApplicationContext(),
						getResources().getString(R.string.aviso_desliga_escuta),
						Toast.LENGTH_SHORT).show();

			}

		}

	}

	private void enviaSMS(String numeroTelefone, String mensagem) {
		SmsManager sms = SmsManager.getDefault();
		sms.sendTextMessage(numeroTelefone, null, mensagem, null, null);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.pro_auto, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		switch (item.getItemId()) {

		case R.id.menu_ajustes:
			startActivityForResult(new Intent("com.msk.proauto.AJUSTES"), 0);
			break;
		case R.id.menu_sobre:
			startActivity(new Intent("com.msk.proauto.SOBRE"));
			break;

		}

		return false;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == RESULT_OK) {
			buscaPreferencias = PreferenceManager
					.getDefaultSharedPreferences(this);
			UsaNumeroCHIP = buscaPreferencias.getBoolean("numero", false);
			numeroCHIP = buscaPreferencias.getString("chip", "");

			if (UsaNumeroCHIP == true) {
				numero.setText(numeroCHIP);
			}
		}
	}

}
