package com.msk.proauto;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceScreen;
import android.widget.Toast;

public class Ajustes extends PreferenceActivity implements
		OnPreferenceChangeListener, OnPreferenceClickListener {

	private Preference versao;
	private EditTextPreference chip;
	private CheckBoxPreference numero;
	private PreferenceScreen prefs;
	private String nrVersao, chave;
	private PackageInfo info;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.preferencias);

		prefs = getPreferenceScreen();

		versao = (Preference) prefs.findPreference("versao");
		chip = (EditTextPreference) prefs.findPreference("chip");
		numero = (CheckBoxPreference) prefs.findPreference("numero");

		try {
			info = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		nrVersao = info.versionName;

		versao.setSummary(getResources().getString(
				R.string.pref_descricao_versao, nrVersao));

		if (numero.isChecked()) {
			numero.setSummary(getResources().getString(R.string.pref_descricao_usa_numero));
		} else {
			numero.setSummary(getResources().getString(R.string.pref_descricao_nao_usa_numero));
		}
		
		numero.setOnPreferenceClickListener(this);
		chip.setOnPreferenceChangeListener(this);

	}

	@Override
	public boolean onPreferenceChange(Preference itemPref, Object numeroCHIP) {

		chave = itemPref.getKey();

		if (chave.equals("chip")) {

			Toast.makeText(
					getApplicationContext(),
					getResources().getString(R.string.aviso_numero_chip,
							numeroCHIP.toString()), Toast.LENGTH_SHORT).show();

			chip.setText(numeroCHIP.toString());
		}

		setResult(RESULT_OK);

		return false;
	}

	@Override
	public boolean onPreferenceClick(Preference itemPref) {
		
		chave = itemPref.getKey();
		
		if (chave.equals("numero")) {
			
			if (numero.isChecked()) {
				numero.setSummary(getResources().getString(R.string.pref_descricao_usa_numero));
			} else {
				numero.setSummary(getResources().getString(R.string.pref_descricao_nao_usa_numero));
			}
			
		}
		
		return false;
	}

}
